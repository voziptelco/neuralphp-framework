<!DOCTYPE html>
	<html lang="es">
		<head>
			<meta charset="utf-8">
			<title>.:: Neural PHP [ Marco de Trabajo en PHP ] ::.</title>
			<meta name="viewport" content="width=device-width, initial-scale=1.0" />
			<?php echo NeuralScriptAdministrador::OrganizarScript(array('CSS' => array('BOOSTRAP', 'DOCS', 'OTROS')), false, 'DEFAULT'); ?>	
			
			<!-- IE6-8 support of HTML5 elements -->
			<!--[if lt IE 9]>
				<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
			<?php echo NeuralScriptAdministrador::OrganizarScript(array('JS' => array('JQUERY', 'BOOTSTRAP', 'APLICACION')), false, 'DEFAULT'); ?>			
		</head>

	<body>
		
		<header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
		  <div class="container">
		    <div class="navbar-header">
		      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
      			<a href="http://www.neuralframework.com/" class="navbar-brand">NEURAL PHP FRAMEWORK </a>
		    </div>    
		  </div>
		</header>

		<div class="bs-header" id="content">
	      <div class="container animated fadeInLeft">
	        <h1>Neural PHP Framework</h1>
	        <p>Bienvenidos a Neural PHP Framework Marco de Trabajo en PHP.</p>
	        <div id="carbonads-container"><div class="logo"><div id="azcarbon"></div>
	        	<img class="animated wobble" src="<?php echo __NeuralUrlRoot__.'Public/images/nf_header.png' ?>"/></div></div>
	      </div>
	    </div>

	<div class="container bs-docs-container">
      <div class="row">
        <div class="col-md-3">
          <div class="bs-sidebar hidden-print" role="complementary">
            <ul class="nav bs-sidenav">
				<li>
				  <a href="#requerimientos">Requerimientos B�sicos</a>
				</li>
				<li>
				  <a href="#validacion">Validaci�n de Requerimientos</a>
				</li>
            </ul>
          </div>
        </div>
		

<div class="col-md-9" role="main">
  
  <div class="bs-docs-section">
  
  	<br />
  	<div class="alert alert-success">
    	<h5><span class="glyphicon glyphicon-ok-sign"></span> &nbsp;<strong>Neural PHP Framework</strong>, Funciona Correctamente.</h5>
    </div>
    <br />
    <p>Neural PHP Framework funciona en tu servidor, ahora podras iniciar el desarrollo de las aplicaciones que desee crear, el cielo es el limite.</p>
    <h4><a>Documentaci�n</a></h4>
	<p>No olvides consultar la <a href="http://www.neuralframework.com" target="_blank">documentaci�n Oficial</a></p>
	<br />
    <h3 id="requerimientos">Requerimientos B�sicos</h3>
    <hr />
    <br />
    <p>Para el buen funcionamiento de Neural PHP Framework se requieren las siguientes condiciones:</p>

		<h4><a>Versi�n de PHP:</a></h4>
		<p>El Framework requiere una versi�n de PHP igual � superior a <code>5.2.3</code>.</p>
		<h4><a>Librerias Necesarias:</a></h4>
		<p>Se requieren las Librerias <code>ctype_alpha</code> y <code>Mcrypt</code>.</p>
		<h4><a>Carpeta de Cache</a></h4>
		<p>Si requiere utilizar la libreria de cache simple es necesario que la carpeta de cahce tenga permisos de lectura y escritura <code>777</code>.</p>
		<h4><a>Extensi�n PDO</a></h4>
		<p>Se requiere la extensi�n PDO para poder realizar las conexiones a las bases de datos, adicionalmente debe tener activo el driver correspondiente al motor de bases de datos correcto.</p>
		<h4><a>Apache Mod_Rewrite</a></h4>
		<p>Es requerido y obligatorio tener activa la opci�n del <code>Mod_Rewrite</code> de Apache.</p>
	<br />


    <h3 id="validacion">Validaci�n de Requerimientos</h3>
    <hr />
    <br />
    <p>Comprobaci�n Automatica de los servicios instalados en el servidor.</p>
	
	<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
		<span class="glyphicon glyphicon-th-list"></span>
  		&nbsp;Comprobar Elementos Instalados
	</button>
    
</div>
</div>
</div>
	
	<footer class="bs-footer" role="contentinfo">
      <div class="container">
        <p>Realizado por Neural PHP Framework.</p>
        <p>Bajo la licencia <a href="http://www.gnu.org/licenses/gpl-3.0.html" target="_blank">GNU/GPL v3.0</a></p>
      </div>
    </footer>
	
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h5 class="modal-title" id="myModalLabel">Comprobacion de Requerimientos</h5>
      </div>
      <div class="modal-body">
        <dl class="dl-horizontal">
		<dt><a>Versi�n de PHP</a></dt>
			<dd><?php AyudasRequerimientos::ValidarVersionPHP(); ?></dd>
		<dt><a>Extensi�n ctype_alpha</a></dt>
			<dd><?php AyudasRequerimientos::ValidarCtypeAlpha(); ?></dd>
		<dt><a>Extensi�n MCrypt</a></dt>
			<dd><?php AyudasRequerimientos::ValidarMCrypt(); ?></dd>
		<dt><a>Directorio Cache</a></dt>
			<dd><?php AyudasRequerimientos::EscribirEnCache(); ?></dd>
		<dt><a>Extensi�n PDO</a></dt>
			<dd><?php AyudasRequerimientos::ValidarPDO(); ?></dd>
		</dl>
      </div>
      
    </div>
  </div>
</div>
		
</body>
</html>